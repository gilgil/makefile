makefile
===

* You can extract all make options by "make -p" command([makefile-all.txt](makefile-all.txt)).

* The important options are as follows([makefile-simple.txt](makefile-simple.txt)).

|Symbol|Description|Example|
|-|-|-|
|CC|C compiler|cc|
|CXX|C++ compiler|g++|
|CFLAGS|C compile and link options|-O2 -I../include -g|
|CXXFLAGS|C++ compile and link options||
|CPPFLAGS|C, C++ compile and link options||
|LDFLAGS|Link options|-L../lib|
|LDLIBS|Link symbols|-lpcap|
|TARGET_ARCH|Architecture options|target armv7a-linux-androideabi21|
